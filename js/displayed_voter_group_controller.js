function generate_voter_group_list(numberOfElements){
  var up_to_first_element = voter_group_button() + '<div name="voter_group_list" class="contact_group_list"><table width="90%" cellspace="2" border="0"><tbody>'+ create_first_voter_group_element();
  for(var i = 2; i<=numberOfElements; i++){
    up_to_first_element += create_voter_group_element(i);
  }
  var final_element = up_to_first_element;
  return final_element +'</tbody></table></div>';
}

function create_first_voter_group_element(){
  return '<tr><th class="td1" nowrap="" width="10%" align="left"><span onclick ="increase_number_of_group_list_elements()" class="fa fa-plus-circle"></span><span onclick = "decrease_number_of_group_list_elements()" class="fa fa-minus-circle"></span><select name="request_op_0"><option value=""></option><option value="NOT">NOT</option></select></th> <td class="td1" id="small"> <select name="find_code_1">' + generate_voter_group_options() +'</select></td></tr>';
}

function create_voter_group_element(index){
  var indexMinusOne = index-1;
  return '<tr><th class="td1" align="left"><select name="request_op_' + indexMinusOne + '"><option value="AND">AND</option> <option value="OR">OR</option> <option value="NOT">NOT</option></select></th>' + '<td class="td1" id="small"> <select name="find_code_' + index + '">' +  "<option>All Contacts</option>" + generate_voter_group_options() + '</select></td> </tr>';
}

function increase_number_of_group_list_elements(){
  //We want to figure out how many elements we have
  for(var i = 1; i < 30; i++){
    var does_exist = document.getElementsByName("find_code_" + i);
    if(does_exist.length != ""){
          }
    else{
      document.getElementById("info_legend").innerHTML = generate_voter_group_list(i);
      //Stop the element from closing after the innerHTML has changed
      var voter_group_list = document.getElementsByName("voter_group_list");
      voter_group_list[0].style.display = "block";
      //Make sure that the close button text does not change
      document.getElementsByName("voter_group_button")[0].innerHTML = "Click to close and load markers!";
      break;
    }
  }
}

//This function is called when we click the minus button corresponding to decreasing the number of group_list_elements
function decrease_number_of_group_list_elements(){
  //We want to figure out how many elements we have
  for(var i = 1; i < 30; i++){
    var does_exist = document.getElementsByName("find_code_" + i);
    if(does_exist.length != ""){}
    else{
      if(i-2 == 0){break;}
      document.getElementById("info_legend").innerHTML = generate_voter_group_list(i-2);
      //We are going to use the voter_group_button_on_click to open the group_list
      var voter_group_list = document.getElementsByName("voter_group_list");
      voter_group_list[0].style.display = "block";
      document.getElementsByName("voter_group_button")[0].innerHTML = "Click to close and load markers!";
      break;
    }
  }
}


function generate_voter_group_options(){
  innerHTML = "";
  CRM.api3('Group', 'get', {
    "sequential": 1,
    "options": {"limit":0},
    "options": {"sort":{"sort":"name ASC"}}
  }).done(function(result) {
    // do something
    for(var i=0; i<result.values.length; i++){
      // This is for excluding turfs
      if(result.values[i].name.slice(0,21) != "Group_created_at_time"){
        innerHTML += "<option data-id =" + result.values[i].id+ ">" + result.values[i].title + "</option>";
      }
    }
  });
  return innerHTML;
}

function voter_group_button(){
  return '<div class="contact_group_button"><button name="voter_group_button" class="voter_group_button" type="button" onclick = voter_group_button_on_click(this)>No Group Selected, Click Me!</button></div>';
}

function voter_group_button_on_click(element){
  //In the case that we are closed we want to open
  if(element.innerText != "Click to close and load markers!"){
    element.innerText = "Click to close and load markers!";
    //Make the voter_group_element appear by changing the display property
    var voter_group_list = document.getElementsByName("voter_group_list");
    voter_group_list[0].style.display = "block";
      }
  //In the case that we are open we want to close
  else{
    element.innerText = "ClickMe!";
    voter_group_list = document.getElementsByName("voter_group_list");
    //Close the voter_group_element by changing the display property to nothing
    voter_group_list[0].style.display = null;
    //Update button to have name of the new groups and their commands
    element.innerText = "This is the name of a group";
    //We are going to generate the markers
    voter_group_list_onsubmit();
  }
}

function voter_group_list_onsubmit(){
  var found = find_form_and_op_elements();
  var forms = found[0];
  var bools = found[1];
  //Add a please wait/ loading text to button
  document.getElementsByName("voter_group_button")[0].innerText = "Please wait, calculating...";
  //First we want to check if we only have one form and operation
  if(forms.length==1){
    var groupname = single_form_and_bool_marker_generator(forms[0],bools[0]);
  }
  else{
    //In the event that we have multiple forms and operations we will handle it recursively
    new_group = new two_group_logic_statement(forms[0],forms[1],bools[0],bools[1]);
    new_group.update_tempgroup(1);
    new_group.compute_logic();
    for(var i = 2; i < forms.length;i++){
      new_group = new two_group_logic_statement(new_group.next_tempgroup_string(),forms[i],"",bools[i]);
      new_group.update_tempgroup(i);
      new_group.compute_logic();
    }
    cleanup_and_display_marker(forms.length);
    document.getElementsByName("voter_group_button")[0].innerText = "Custom query with groups " + forms.join() + " and operations " + bools.join();
  }
}

function find_form_and_op_elements(){
  var form1 = document.getElementsByName("find_code_1");
  var bool0 = document.getElementsByName("request_op_0");
  var bools = [bool0[0].selectedOptions[0].innerText];
  var forms = [form1[0].selectedOptions[0].value];
  var current_form = document.getElementsByName("find_code_2");
  var current_bool = document.getElementsByName("request_op_1");
  while(current_form.length != 0){
    var current_form_value = current_form[0].selectedOptions[0].value;
    var current_bool_value = current_bool[0].selectedOptions[0].value;
    forms.push(current_form_value);
    bools.push(current_bool_value);
    var form_length_plus_one = forms.length+1;
    var form_length_minus_one = forms.length-1;
    var find_code_string = "find_code_" + form_length_plus_one;
    current_bool = document.getElementsByName("request_op_"+form_length_minus_one);
    current_form = document.getElementsByName(find_code_string);
  }
  return [forms,bools];
}

function cleanup_and_display_marker(numberOfTempGroups){
  //Create markers and update voters
  var group_string = "tempgroup_" + numberOfTempGroups;
  CRM.api3('Contact', 'get', {
    "sequential": 1,
    "group": group_string,
    "options": {"limit":0}
  }).done(function(result) {
    civi_contact_results_handler(result);
  });
  //Delete all of the created groups
  for(var i = 2; i <= numberOfTempGroups; i++){
    var title_string = "tempgroup_" + i;
    console.log(title_string);
    CRM.$.ajaxSetup({async: false}); //NOTE TO SELF! Get rid of this line and replace with ajax promises
    CRM.api3('Group', 'getsingle', {
      "debug": 1,
      "title": title_string,
    }).done(function(result) {
      // do something
      CRM.api3('Group', 'delete', {
        "debug": 1,
        "id": result.id
      }).done(function(result) {
        // do something
      });
    });
  }
}

function single_form_and_bool_marker_generator(form,bool){
  if(bool == ""){
    //Create group
    CRM.api3('Contact', 'get', {
      "debug" : 1,
      "sequential": 1,
      "group": form,
      "options": {"limit":0}
    }).done(function(result) {
      civi_contact_results_handler(result);
    });
    //We want to change the text within the button to that of the group
    document.getElementsByName("voter_group_button")[0].innerText = form;
  }
  else if(bool == "NOT"){
    CRM.api3('Contact', 'get', {
      "sequential": 1,
      "group": {"NOT IN":[form]},
      "groups": {"IS NULL":1},
      "options": {"limit":0}
    }).done(function(result) {
      civi_contact_results_handler(result);
      });
    document.getElementsByName("voter_group_button")[0].innerText = "NOT " + form;
  }
  else{
    console.log("Something is wrong in single_form_and_bool_generator, bool");
  }
}

function civi_contact_results_handler(result){
  // do something
  voters.clearLayers();
  var defaultMarker = L.ExtraMarkers.icon({
    icon: 'fa-circle',
    iconColor: "white",
    markerColor: 'blue',
    shape: 'circle',
    prefix: 'fa'
  });
  for(var i=0; i<result.count; i++){
    var location = [result.values[i].geo_code_1,result.values[i].geo_code_2];
    var marker = L.marker(location, {
      "id" : result.values[i].id,
      icon : defaultMarker
    });
    marker.bindPopup("<div style='text-align: center; margin-left: auto; margin-right: auto;'>"+ result.values[i].first_name + " " + result.values[i].last_name + " , " + result.values[i].street_address + " , " + result.values[i].city + " , " + result.values[i].postal_code + "</div>", {maxWidth: '400'});
    marker.ID = result.values[i].id;
    marker.language = result.values[i].preferred_language;
    voters.addLayer(marker);
  }
}


function convert_contact_result_to_ids(result){
  var returnable = [];
  console.log(result);
  for(var i = 0; i < result.count; i++){
    returnable.push(result.values[i].contact_id);
  }
  return returnable;
}

function convert_contact_responseJSON_to_ids(result){
  var returnable = [];
  console.log(result);
  var responseJSON_values = result.responseJSON.values;
  for(var i = 0; i < responseJSON_values.length; i++){
    returnable.push(responseJSON_values[i].contact_id);
  }
  return returnable;
}


/*
This class allows for the creation of a logic statement on two groups. For example,
not A or B is a valid logic statement on two groups. There is an amount of ambiguity that comes with logic statements. For example, what does A not B mean? In cases such as this I default to what makes sense from an english reading perspective (for example, A not B becomes A AND not B)
*/
class two_group_logic_statement {
  constructor(group1,group2,operation1="",operation2){
    this.group1 = group1;
    this.group2 = group2;
    this.operation1 = operation1;
    this.operation2 = operation2;
    this.tempgroup = "";
  }
  //This function will be run to delete the created temporary group
  destructor(){
    CRM.api3('Group', 'getsingle', {
      "name": this.tempgroup
    }).done(function(result) {
      // We have the group, let's delete it based on the id
      CRM.api3('Group', 'delete', {
        "id": result.id
      }).done(function(result) {
        // do something
      });
    });
  }
  update_tempgroup(group_number){
    this.tempgroup = "tempgroup_" + group_number;
  }
  next_tempgroup_string(){
    var poped = this.tempgroup.split("_").pop();
    var poped_plus_one = parseInt(poped) + 1;
    var toReturn = this.tempgroup.split("_")[0] + "_" + poped_plus_one;
    return toReturn;
  }
  create_next_group(){
    var next_tempgroup_name = this.next_tempgroup_string();
    CRM.api3('Group', 'create', {
      "title": next_tempgroup_name
    }).done(function(result) {
      // do something
    });
  }
  create_current_group(){
    var current_group_name = this.tempgroup;
    CRM.api3('Group', 'create', {
      "title": next_tempgroup_name
    }).done(function(result) {
      // do something
    });
  }

  current_group_id(){
    var returnable = CRM.api3('Group', 'get', {
      "sequential": 1,
      "title": this.tempgroup
    });
    return returnable.responseJSON.id;
  }

  group_id_to_name(id){
    var returnable = CRM.api3('Group', 'getsingle', {
      "id": id
    });
    return returnable.responseJSON.title;
  }

  next_group_id(){
    var next_tempgroup_name = this.next_tempgroup_string();
    var results = CRM.api3('Group', 'get', {
      "sequential": 1,
      "title": next_tempgroup_name
    });
    return results.responseJSON.id;
  }

  //There are several functions we are going to write on a logic statement of two groups.
  //The main function will be compute_logic(outputgroup) which will compute the stated logic and make it available at the name of the output group, more documentation available below

  //This main function will call from many smaller functions in order to complete it's ask, namely a logic parser which preforms a large if/ else statement in order to consider all of the cases available and call the appropriate handling function

  compute_logic(){
    //We use switch of true because we will use output parameters anyways
    switch(true){
      //A and B

      //Note we will never have to all_contacts in the first position. This is also incredibly bad practice

    case(this.group2=="All Contacts" && this.operation1=="" && this.operation2=="AND"):
      this.any_all_contacts_none_and_handler();
      break;
    case(this.operation1=="" && this.operation2=="AND"):
      this.any_any_none_and_handler();
      break;

      //A or B

    case(this.group2=="All Contacts" && this.operation1=="" && this.operation2=="OR"):
      this.any_all_contacts_none_or_handler();
      break;
    case(this.operation1=="" && this.operation2=="OR"):
      this.any_any_none_or_handler();
      break;

      //A or NOT B

    case(this.group2=="All Contacts" && this.operation1=="" && this.operation2=="NOT"):
      this.any_all_contacts_none_not_handler();
      break;
    case(this.operation1=="" && this.operation2=="NOT"):
      this.any_any_none_not_handler();
      break;

      //NOT A and B

    case(this.group2=="All Contacts" && this.operation1=="NOT" && this.operation2=="AND"):
      this.any_all_contacts_not_and_handler();
      break;
    case(this.operation1=="NOT" && this.operation2=="AND"):
      this.any_any_not_and_handler();
      break;

      //NOT A or B

    case(this.group2=="All Contacts" && this.operation1=="NOT" && this.operation2=="OR"):
      this.any_all_contacts_not_or_handler();
      break;
    case(this.operation1=="NOT" && this.operation2=="OR"):
      this.any_any_not_or_handler();
      break;

      //NOT A or NOT B

    case(this.group2=="All Contacts" && this.operation1=="NOT" && this.operation2=="NOT"):
      this.any_all_contacts_not_not_handler();
      break;
    case(this.operation1=="NOT" && this.operation2=="NOT"):
      this.any_any_not_not_handler();
      break;
    default :
      console.log("Entered switch statement default something is wrong");
      console.log("group1",this.group1,"group2",this.group2,"operation1",this.operation1,"operation2",this.operation2);
    }
  }

  any_all_contacts_none_and_handler(){
    //Pretty much the same as all_contacts_any_none_and_handler
    //This is the same as just putting any into the next group
    var nextGroup = this.next_tempgroup_string();
    this.create_next_group();
    var next_group_id = this.next_group_id();
    var current_group_id = this.current_group_id();
    var current_group_name = this.group_id_to_name(current_group_id);
    CRM.api3('Group', 'get', {
      "debug": 1,
      "sequential": 1,
      "options": {"limit":0}
    }).done(function(result) {
      // do something
      console.log(result);
    });
    CRM.$.ajaxSetup({async : false});
    console.log(current_group_name);
    CRM.api3('Contact', 'get', {
      "debug" : 1,
      "sequential": 1,
      "group": {"IN":[this.group1,this.group1]},
      "options": {"limit":0}
    }).done(function(result) {
      // create a list of contact ids from the result
      var contact_id_list = convert_contact_result_to_ids(result);
      CRM.api3('GroupContact', 'create', {
        "group_id": next_group_id,
        "contact_id": contact_id_list
      }).done(function(result) {
        // do something
      });
    });
  }

  any_any_none_and_handler(){
    var nextGroup = this.next_tempgroup_string();
    this.create_next_group();
    var next_group_id = this.next_group_id();
    //Get all contact ids within the first group
    var result = CRM.api3('Contact', 'get', {
      "sequential": 1,
      "group": this.group1,
      "options": {"limit":0}
    });
    var contact_ids_in_group1 = convert_contact_responseJSON_to_ids(result);
    //Get all contact ids within the second group
    result = CRM.api3('Contact', 'get', {
      "sequential": 1,
      "group": this.group2,
      "options": {"limit":0}
    });
    var contact_ids_in_group2 = convert_contact_responseJSON_to_ids(result);
    //Find the intersection of these two lists
    var intersection_ids = contact_ids_in_group1.filter(value => -1 !== contact_ids_in_group2.indexOf(value));
    //Create the next group
    CRM.api3('GroupContact', 'create', {
      "group_id": next_group_id,
      "contact_id": intersection_ids
    });
}

  any_all_contacts_none_or_handler(){
    //This is just all contacts
    var nextGroup = this.next_tempgroup_string();
    this.create_next_group();
    var next_group_id = this.next_group_id();
    CRM.api3('Contact', 'get', {
      "sequential": 1,
      "options": {"limit":0}
    }).done(function(result) {
      // do something
      var contact_id_list = convert_contact_result_to_ids(result);
      CRM.api3('GroupContact', 'create', {
        "group_id": next_group_id,
        "contact_id": contact_id_list
      });
    });
  }

  any_any_none_or_handler(){
    var nextGroup = this.next_tempgroup_string();
    this.create_next_group();
    var next_group_id = this.next_group_id();
    //Get all contact ids within the first group
    var result = CRM.api3('Contact', 'get', {
      "sequential": 1,
      "group": this.group1,
      "options": {"limit":0}
    });
    var contact_ids_in_group1 = convert_contact_responseJSON_to_ids(result);
    //Get all contact ids within the second group
    result = CRM.api3('Contact', 'get', {
      "sequential": 1,
      "group": this.group2,
      "options": {"limit":0}
    });
    var contact_ids_in_group2 = convert_contact_responseJSON_to_ids(result);
    //Find the union of these two lists
    var union_ids_temp = contact_ids_in_group1.concat(contact_ids_in_group2);
    let union_ids = [...new Set(union_ids_temp)];
    //Create the next group
    CRM.api3('GroupContact', 'create', {
      "group_id": next_group_id,
      "contact_id": union_ids
    });

  }

  any_all_contacts_none_not_handler(){
    //This is the same thing as any contact
    var nextGroup = this.next_tempgroup_string();
    this.create_next_group();
    var next_group_id = this.next_group_id();
    CRM.api3('Contact', 'get', {
      "sequential": 1,
      "return": ["id"],
      "group": this.group1,
      "options": {"limit":0}
    }).done(function(result) {
      // do something
      var contact_id_list = convert_contact_result_to_ids(result);
      CRM.api3('GroupContact', 'create', {
        "group_id": next_group_id,
        "contact_id": contact_id_list
      }).done(function(result) {
        // do something
      });
    });
  }

  any_any_none_not_handler(){
    //This is any interesting case, this is A and NOT B
    var nextGroup = this.next_tempgroup_string();
    this.create_next_group();
    var next_group_id = this.next_group_id();
    //Get all contact ids within the first group
    var result = CRM.api3('Contact', 'get', {
      "sequential": 1,
      "group": this.group1,
      "options": {"limit":0}
    });
    var contact_ids_in_group1 = convert_contact_responseJSON_to_ids(result);
    //Get all contact ids within the second group
    result = CRM.api3('Contact', 'get', {
      "sequential": 1,
      "groups": {"!=":this.group2},
      "options": {"limit":0}
    });
    var contact_ids_in_group2 = convert_contact_responseJSON_to_ids(result);
    //Find the intersection of these two lists
    var intersection_ids = contact_ids_in_group1.filter(value => -1 !== contact_ids_in_group2.indexOf(value));
    //Create the next group
    CRM.api3('GroupContact', 'create', {
      "group_id": next_group_id,
      "contact_id": intersection_ids
    });
  }
  any_all_contacts_not_and_handler(){
    //This is the same thing as saying not any
    var nextGroup = this.next_tempgroup_string();
    this.create_next_group();
    var next_group_id = this.next_group_id();
    CRM.api3('Contact', 'get', {
      "sequential": 1,
      "return": ["id"],
      "group": {"!=":this.group1},
      "options": {"limit":0}
    }).done(function(result) {
      // do something
      var contact_id_list = convert_contact_result_to_ids(result);
      CRM.api3('GroupContact', 'create', {
        "group_id": next_group_id,
        "contact_id": contact_id_list
      }).done(function(result) {
        // do something
      });
    });
  }

  any_any_not_and_handler(){
    //This is not A and B
    var nextGroup = this.next_tempgroup_string();
    this.create_next_group();
    var next_group_id = this.next_group_id();
    //Get all contact ids within the first group
    var result = CRM.api3('Contact', 'get', {
      "sequential": 1,
      "groups": {"IS NOT NULL":1},
      "options": {"limit":0}
    });
    var all_contacts  = convert_contact_responseJSON_to_ids(result);
    Array.prototype.diff = function(a) {
      return this.filter(function(i) {return a.indexOf(i) < 0;});
    };
    //Get the contacts within group1
    result = CRM.api3('Contact', 'get', {
      "sequential": 1,
      "group": this.group1,
      "options": {"limit":0}
    });
    var contact_ids_in_group1 = convert_contact_responseJSON_to_ids(result);

    contact_ids_in_group1 = all_contacts.diff(contact_ids_in_group1);
    //Get all contact ids within the second group
    var result2 = CRM.api3('Contact', 'get', {
      "sequential": 1,
      "group": this.group2,
      "options": {"limit":0}
    });
    var contact_ids_in_group2 = convert_contact_responseJSON_to_ids(result2);
    console.log(contact_ids_in_group1);
    console.log(contact_ids_in_group2);
    //Find the intersection of these two lists
    var intersection_ids = contact_ids_in_group1.filter(value => -1 !== contact_ids_in_group2.indexOf(value));
    //Create the next group
    CRM.api3('GroupContact', 'create', {
      "group_id": next_group_id,
      "contact_id": intersection_ids
    });
  }

  any_all_contacts_not_or_handler(){
    //This is just all contacts
    var nextGroup = this.next_tempgroup_string();
    this.create_next_group();
    var next_group_id = this.next_group_id();
    CRM.api3('Contact', 'get', {
      "sequential": 1,
      "return": ["id"],
      "options": {"limit":0}
    }).done(function(result) {
      // do something
      var contact_id_list = convert_contact_result_to_ids(result);
      CRM.api3('GroupContact', 'create', {
        "group_id": next_group_id,
        "contact_id": contact_id_list
      }).done(function(result) {
        // do something
      });
    });
  }

  any_any_not_or_handler(){
    //This is just all contacts
    var nextGroup = this.next_tempgroup_string();
    this.create_next_group();
    var next_group_id = this.next_group_id();
    CRM.api3('Contact', 'get', {
      "sequential": 1,
      "return": ["id"],
      "options": {"limit":0}
    }).done(function(result) {
      // do something
      var contact_id_list = convert_contact_result_to_ids(result);
      CRM.api3('GroupContact', 'create', {
        "group_id": next_group_id,
        "contact_id": contact_id_list
      }).done(function(result) {
        // do something
      });
    });
  }

  any_all_contacts_not_not_handler(){
    //This just creates an empty group
    create_next_group();
  }

  any_any_not_not_handler(){
    //This is all contacts that are not within these two groups
    var nextGroup = this.next_tempgroup_string();
    this.create_next_group();
    var next_group_id = this.next_group_id();
    //Get all contact ids within the first group
    var result = CRM.api3('Contact', 'get', {
      "sequential": 1,
      "groups": {"IS NOT NULL":1},
      "options": {"limit":0}
    });
    var all_contacts  = convert_contact_responseJSON_to_ids(result);
    Array.prototype.diff = function(a) {
      return this.filter(function(i) {return a.indexOf(i) < 0;});
    };
    //Get the contacts within group1
    result = CRM.api3('Contact', 'get', {
      "sequential": 1,
      "group": this.group1,
      "options": {"limit":0}
    });
    var contact_ids_in_group1 = convert_contact_responseJSON_to_ids(result);

    //Get all contact ids within the second group
    var result2 = CRM.api3('Contact', 'get', {
      "sequential": 1,
      "group": this.group2,
      "options": {"limit":0}
    });
    var contact_ids_in_group2 = convert_contact_responseJSON_to_ids(result2);
    //First we will find the union of the two groups and then find the difference between all contacts and the groups
    var union_ids_temp = contact_ids_in_group1.concat(contact_ids_in_group2);
    let union_ids = [...new Set(union_ids_temp)]; //note this returns an array not a set
    //Create the next group
    CRM.api3('GroupContact', 'create', {
      "group_id": next_group_id,
      "contact_id": all_contacts.diff(union_ids)
    });
  }
}
