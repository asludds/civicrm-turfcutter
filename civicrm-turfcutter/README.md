# CiviCRM Turfcutter

Currently one of the most time consuming tasks for a political campaign is to be able to "cut-turf". This is the process by which a staffer or campaign manager assigns volunteers to a subsection of voters for canvassing. Similar practices are used in other fields such as door-to-door fundraising and census data collection.


<!---
## Getting Started

### Installation 
TBD


### Running the Tests
TBD
-->

## Architecture
The underlying architecture relies on API calls to CiviCRM as well as using leaflet to manage maps and plotting icons on maps.

The following diagram describes the expected flows of data that a user of the turfcutter software will use when interacting with the application.


![Architecture](architecture.png)

### Functions
There are several key functions which will define the use of this application. The most complex functions are listed here:

detectClosedPolygon: Given the locations of several markers, returns a boolean representing if these markers are a closed polygon or not. It may be possible to us the following leaflet extension, but likely a faster algorithm will be needed : "https://github.com/mapbox/leaflet-pip"

generateWalkList: Given a volunteers turf, make API calls to google maps to find the optimal walk route. 

generateVoterMap: This gets the latitudes, longitudes, and street addresses of all voters from CiviCRM.

generateVolunteerMap: This gets the latitudes, longitudes, and street addresses of all volunteers from CiviCRM.

generateHeatMap: This function takes several different statistical parameters and queries the voters to find which voters fit these statistical parameters. For example, a query could be made such as "show voters which have been canvassed by Susan Hopkins in the last two weeks". In this way field-managers can have visual understanding of their voters information better.

### User Interface

The final user interface for the project will look like this:

![leaflet](leafletdemo.png)

This user interface is taken from the following project:
"https://github.com/firechicago/turf-cutter"

#### Technologies 

For the creation of the user interface several different technologies will be used in addition to civiCRM and google maps API.
Leaflet, a javascript interactive maps libary, will be heavily utilized for displaying and interacting with maps. "https://leafletjs.com/"

Leaflet.SlideMenu is an extension for leaflet which allows for the creation of sliding menus such as those that we will see on the left and right side of our application. "https://github.com/unbam/Leaflet.SlideMenu"

Leaflet.contextMenu is an extension for leaflet which allows for the creation of appearing menus which allows for the user to right click and be presented with a menu giving additional options with how to interact with the objects they are hovering over. "https://github.com/aratcliffe/Leaflet.contextmenu"

Though technically not an extension, it is worth noting that leaflet has a layers feature which will be used for implementing the voter view, volunteer view, and heatmap. "https://leafletjs.com/examples/layers-control/"

Leaflet.toolbar is an extension of leaflet which allows for the integration of toolbars into the leaflet application. "https://github.com/Leaflet/Leaflet.toolbar"

Something to consider is that we must change icon colors for the heatmap view. A solution for how to do this from a single marker template is found here: "https://leafletjs.com/reference-1.3.0.html#divicon"

Leaflet.Slider is an extension of leaflet which allows for the integration of slides into the leaflet application. "https://github.com/Eclipse1979/leaflet-slider"



#### User Interface Descrption

There are several components to this user interface.

### Center Map
This center maps contains information about geographical locations. While in voter view, this will show markers for voter locations. While in volunteer view, previous turfs of the volunteer will be shown to give context to the end-user. While in heat-map view the markers will change color to correspond to voter information. What type of information this will be can be set in an options menu. For example, you could query the data and ask "what voters have not been canvassed in the last two weeks?" and the color of markers will change accordingly.


### Left Slide In Menu
This menu will contain meta information about the project. For example, a tutorial which describes how different parts of the project can be utilized. An about-us menu for information on contacting the developers and reporting bugs.

### Right Slide in Menu
This menu will be the volunteer statistics menu. Often times in a campaign it is important to see data about which volunteers have turf assigned and which do not have turf assigned. This menu will help to aid in volunteer management. Volunteers who do not have a turf are listed at the top of the menu with green text and volunteers who have a turf are listed at the bottom of the menu with red text. Hovers on a volunteers name will display their current or most recently assigned turf. Clicking on a volunteer will display statistical information such as number of turfs assigned, average number of voters hit per turf and average duration of turf assignment.



